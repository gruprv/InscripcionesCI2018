/* global $ */
$.fn.plantilla_generica_tabla = function () {
	'use strict';
	var element = $(this),
		dataRows = element.find('tbody tr');
	element.find('td').on('change', function (evt) {
		var cell = $(this),
			column = cell.index();
		if (column === 0) {
			return;
		}		
	}).on('validate', function (evt, value) {
		var cell = $(this),
			column = cell.index();
		return !!value && value.trim().length > 0;
	});
	return this;
};
