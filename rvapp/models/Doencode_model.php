<?php 
    if ( ! defined('BASEPATH') ) exit('No direct script access allowed');

    class Doencode_model extends CI_Model {

        protected $logar        = false;
        protected $procesarlog  = "";

        function __construct(){
            parent::__construct();
        } 

        function getCuentaIdiomas() {   
            $sql = "SELECT count(id_inscripcion) AS cuenta 
                    FROM inscripciones_idiomas 
                    WHERE enc = 0"; //
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            if (count($datos) > 0) {
                return ($datos[0]->cuenta);
            }

            return 0;
        }

        function getCuentaColonias() {   
            $sql = "SELECT count(idinscripciones) AS cuenta 
                    FROM inscripciones 
                    WHERE enc = 0"; //
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            if (count($datos) > 0) {
                return ($datos[0]->cuenta);
            }

            return 0;
        }

        function getNextIdiomas($limite) {   
            $sql = "SELECT *
                    FROM inscripciones_idiomas 
                    WHERE enc = 0
                    ORDER BY  id_inscripcion ASC
                    LIMIT ".$limite; //
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            return $datos;
        }

        function getNextColonias($limite) {   
            $sql = "SELECT *
                    FROM inscripciones 
                    WHERE enc = 0
                    ORDER BY  idinscripciones ASC
                    LIMIT ".$limite; //
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            return $datos;
        }

        function getEncColonias($idinscripciones, $idpeople, $clavecontrol) {   
            $sql = "SELECT *
                    FROM inscripciones 
                    WHERE   enc = 1 and
                            idinscripciones = '$idinscripciones' and 
                            idpeople = '$idpeople' and 
                            control = '$clavecontrol' 
                    ORDER BY  idinscripciones ASC"; //
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            return (array)$datos[0];
        }

        function getEncIdiomas($idinscripciones, $idpeople, $clavecontrol) {   
            $sql = "SELECT *
                    FROM inscripciones_idiomas
                    WHERE   enc = 1 and
                            id_inscripcion = '$idinscripciones' and 
                            idpeople = '$idpeople' and 
                            control = '$clavecontrol' 
                    ORDER BY  id_inscripcion ASC"; //
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            return (array)$datos[0];
        }

        function getOneIdiomas($idinscripciones) {   
            $sql = "SELECT *
                    FROM inscripciones_idiomas
                    WHERE id_inscripcion = '$idinscripciones'"; //
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            return $datos;
        }

        function getNextIdiomasAge($next) {
            $sql = "SELECT *
                    FROM inscripciones_idiomas
                    WHERE length(edad_participante) < 5 
                    limit $next"; //
            $query = $this->db->query($sql);
            
            $datos = (array)$query->result();
            
            return $datos;
        }

        function setEncColonias(  $idinscripciones, $idpeople, $clavecontrol, $datos ) {
            $where = "idinscripciones = '$idinscripciones' and idpeople = '$idpeople' and control = '$clavecontrol'";
            $datos["enc"] = 1;

            $this->db->where($where);
            $this->db->update("inscripciones", $datos);
        }

        function setEncIdiomas(  $idinscripciones, $idpeople, $clavecontrol, $datos ) {
            $where = "id_inscripcion = '$idinscripciones' and idpeople = '$idpeople' and control = '$clavecontrol'";
            $datos["enc"] = 1;

            $this->db->where($where);
            $this->db->update("inscripciones_idiomas", $datos);
        }
    }
?>