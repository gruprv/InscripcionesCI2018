<?php 
    /* ponemos este codigo en lo alto de las vistas para asi forzar que podamos acceder a los metodos del controlador padre. <pre><?php echo print_r($privilegios, true); ?></pre> */
$CI = &get_instance();
$img_pdf = base_url() . "UploadedFiles/IMG/pdf150.gif";

function get_value($nombre, $value)
{
    if ((isset($_POST[$nombre])) and ($_POST[$nombre] != "")) {
        return $_POST[$nombre];
    } else {
        return $value;
    }
}

if (!isset($colectiu)) $colectiu = false;
?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "https://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="https://www.w3.org/1999/xhtml">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="google" content="notranslate" />
                <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8 X-Content-Type-Options=nosniff" />
        <meta http-equiv="expires" content="0" />
        <meta http-equiv="Cache-Control" content ="no-cache" />

        <title><?php echo lang("TitolWeb"); ?></title>

        <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
            
        <!-- Bootstrap -->
            <link href="<?php echo base_url(); ?>css/bootstrap/bootstrap.min.css" rel="stylesheet">
            <link href="<?php echo base_url(); ?>css/bootstrap/style-indis.css" rel="stylesheet">
            <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-ui-1.8.16.custom.min.js"></script>

            <link href='https://fonts.googleapis.com/css?family=Roboto:400,100,300,500,700,900' rel='stylesheet' type='text/css'>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css">
            <link href="<?php echo base_url(); ?>favicon.ico" rel="shortcut icon" type="image/x-icon" />

            <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
            <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
            <!--[if lt IE 9]>
            <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
            <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
            <![endif]-->
            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="<?php echo base_url(); ?>jquery/js/bootstrap.min.js"></script>
            <script src="<?php echo base_url(); ?>jquery/js/bootstrap-select.js"></script>
            <!--script type="text/javascript">
                // Igualar alturas de paneles
                $(document).ready(function (){
                    var highestCol = Math.max($('.tabla .celda').innerHeight());
                    $('.tabla .celda').height(highestCol+30);
                });
            </script-->
        <!-- !Bootstrap -->
        <?php
            if ($permitirModificacion) {
                ?><script src="<?php echo base_url(); ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-<?php echo strtolower($lang); ?>.js"></script><?php
            }
        ?>

        <style> 
            .btnSubmit {
                padding: 10px; 
                background-color: #0069af!important; 
                border: 1px solid #0069af!important; 
                border-radius: 5px; 
                font-weight: bold; 
                color: #ffffff !important; 
                font-size: 14px; 
                text-decoration:none; float: right; } 
            .ui-datepicker-year {
                color: rgb(14, 60, 99)!important;
            }
            .minH {
                min-height: 50px!important;
            }
            
            .col-container {
                display: table;
                width: 100%;
                border-spacing: 5px;
                border-collapse: separate;
            }
            .col {
                display: table-cell;
                padding: 16px;
                margin: 5px;
            }
            .btnSubmit {
                bottom: 10px;
            }
        </style>
        <?php
            echo "<style>";
            $posicio = 0;
            foreach ($datos as $dato) {
                $posicio++;            
                ?>
                    .form-group.form<?php echo $posicio; ?>imagen_dnia_th.has-error, .form-group.form<?php echo $posicio; ?>imagen_dnir_th.has-error {
                        border: 1px solid #a94442;
                        border-radius: 4px;
                    }
                        
                    .form-group.form<?php echo $posicio; ?>imagen_dnia_th.has-error .padding5 b, .form-group.form<?php echo $posicio; ?>imagen_dnir_th.has-error .padding5 b {
                        color: #a94442;
                    }
                <?php
            }
            echo "</style>";
        ?>

        <link type="text/css" href="<?php echo base_url(); ?>jquery/css/start/jquery-ui-1.8.16.custom.css" rel="Stylesheet" />   
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-1.6.2.min.js"></script>
        <script type="text/javascript" src="<?php echo base_url(); ?>jquery/js/jquery-ui-1.8.16.custom.min.js"></script>
        
        <?php
        if ($permitirModificacion) {
            ?>
                    <script src="<?php echo base_url(); ?>jquery/development-bundle/ui/i18n/jquery.ui.datepicker-<?php echo strtolower($lang); ?>.js"></script>
                <?php
            }
        ?>

    </head>
    <body>
        <header class="top-header hidden-print">
            <div class="container">
                <a href="#" target="_blank"><h1 class="logo" style="background-image:url(<?php echo base_url(); ?>images/logo_rosadelsvents-indis.png);">Rosa dels Vents</h1></a>
                <div class="support">
                    <p class="question"><?php echo lang("Titol"); ?></p>
                    <p><a href="tel:934092071" class="phone">934092071</a></p>
                    <p><a href="mailto:informacio@rosadelsvents.es" class="email">informacio@rosadelsvents.es</a></p>
                </div> 
            </div>
        </header>

        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-12">
            <div class="padding5 minH">
                <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5 minH">
                    <h1><?php echo lang("Titol_vuelo"); ?></h1>
                </div>
            </div>
            <br/>
            <div class="padding5 minH box border3 boxactive">
                <div class="col-xs-6 step2-desc padding5 padding5 minH" style="line-height:45px;">
                    <b><?php echo lang("NReserva") . "/" . lang("Localitzador"); ?>:</b>
                </div>                    
                <div class="col-xs-6 step2-desc padding5 minH">
                    <input type="text" readonly value="<?php echo $datos[0]->localizador; ?>" id="OrderNumber" name="OrderNumber" class="form-control"/>
                </div>                    
            </div>
            <br/>
            <?php 
                if (validation_errors() != "") {
                    ?>
                        <div class="alert alert-danger">
                            <label><?php echo lang("campos_obligatorios_pendientes"); ?></label>
                        </div>
                        <br /> 
                    <?php
                }
            ?>

            <form action="<?php echo base_url(); ?>inscripcion/guardar_taf/<?php echo $param_url; ?>/<?php echo $lang . ($forzar == true ? "/true" : ""); ?>" method="post" id="formreservation" name="formreservation" class="padding0">
                <?php 
                    $posicio = 0;
                    foreach ($datos as $dato) {
                        $posicio++;

                        ?>
                            <div class="padding5">
                                <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
                                    <h2><?php echo lang("Participant_N"); ?> <?php echo $posicio; ?></h2>
                                </div>
                            </div>
                        <?php

/*                      if ($permitirModificacion) {
                            ?><form method="post" id="formreservation<?php echo $posicio;?>" name="formreservation<?php echo $posicio;?>"  enctype="multipart/form-data"  class="padding0"><?php 
                        } */

                        ?>
                            <input type="hidden" id="form<?php echo $posicio; ?>idep" name="form<?php echo $posicio; ?>idep" value="<?php echo $this->soporte_xss->encode($dato->idpeople); ?>"/>
                            <input type="hidden" id="form<?php echo $posicio; ?>loca" name="form<?php echo $posicio; ?>loca" value="<?php echo $this->soporte_xss->encode($dato->localizador); ?>"/>

                            <div class="padding5 minH box border3" style="margin-bottom:25px;">
                                <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                                    <div class="form-group form<?php echo $posicio; ?>name">
                                        <label class="control-label" for="form<?php echo $posicio; ?>name"><b><?php echo lang("Nom") ?>:</b></label>
                                        <?php
                                            if (!$permitirModificacion) {
                                                echo get_value("name", $dato->name);
                                            } else {
                                                ?>
                                                    <input type="text" id="form<?php echo $posicio; ?>name" name="form<?php echo $posicio; ?>name" class="form-control" maxlength="150" value="<?php echo get_value("name", $dato->name); ?>" onchange="validacion(true);"/>
                                                    <?php echo form_error("form<?php echo $posicio; ?>name"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                                    <div class="form-group <?php echo $posicio; ?>sur1">
                                        <label class="control-label" for="form<?php echo $posicio; ?>sur1"><b><?php echo lang("Cognom1") ?>:</b></label>
                                        <?php
                                            if (!$permitirModificacion) {
                                                echo get_value("sur1", $dato->sur1);
                                            } else {
                                                ?>
                                                    <input type="text" id="form<?php echo $posicio; ?>sur1" name="form<?php echo $posicio; ?>sur1" class="form-control" maxlength="150" value="<?php echo get_value("sur1", $dato->sur1); ?>" onchange="validacion(true);"/>
                                                    <?php echo form_error("form<?php echo $posicio; ?>sur1"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                                    <div class="form-group form<?php echo $posicio; ?>sur2">
                                        <label class="control-label" for="form<?php echo $posicio; ?>sur2"><b><?php echo lang("Cognom2") ?>:</b></label>
                                        <?php
                                            if (!$permitirModificacion) {
                                                echo get_value("sur2", $dato->sur2);
                                            } else {
                                                ?>
                                                    <input type="text" id="form<?php echo $posicio; ?>sur2" name="form<?php echo $posicio; ?>sur2" class="form-control" maxlength="150" value="<?php echo get_value("sur2", $dato->sur2); ?>" onchange="validacion(true);"/>
                                                    <?php echo form_error("form<?php echo $posicio; ?>sur2"); ?>
                                                <?php 
                                            }
                                        ?>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                                    <div class="form-group born">
                                        <label class="control-label" for="form<?php echo $posicio; ?>born"><b><?php echo lang("DataNaixement") ?>:</b></label>
                                        <?php
                                            if (!$permitirModificacion) {
                                                echo set_value("born", $CI->soporte_general->cambia_fecha($dato->born, "-", "/"));
                                            } else { 
                                                    // echo "<pre>FN:  ".set_value("born", $dato->born)." - ".$dato->born."</pre>";
                                                $born = set_value("born", $CI->soporte_general->cambia_fecha($dato->born, "-", "/"));
                                                ?><input type="text" id="form<?php echo $posicio; ?>born" name="form<?php echo $posicio; ?>born" readonly="readonly" maxlength="150" class="form-control input calendario" value="<?php echo set_value("born", $born); ?>"  onchange="validacion(true);"/><?php 
                                                echo form_error("form<?php echo $posicio; ?>born");
                                            }
                                        ?>
                                        <script type="text/javascript">
                                            $(function() {
                                                $('#form<?php echo $posicio; ?>born').datepicker({
                                                    dateFormat: 'dd/mm/yy', 
                                                    showWeek: true,
                                                    firstDay: 1,
                                                    numberOfMonths: 1,
                                                    changeYear: true,
                                                    regional: $.datepicker.regional['<?php echo strtolower($lang); ?>'], 
                                                    showOtherMonths: true,
                                                    selectOtherMonths: true
                                                })<?php
                                                    if ($born == "01/01/1800") {
                                                        // echo '.datepicker("setDate", "01/01/' . $z_Anyodesde . '")';
                                                    } else {
                                                        echo '.datepicker("setDate", "' . $born . '")';
                                                    }
                                                ?>;
                                            });
                                        </script>
                                    </div>
                                </div>
                                <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                                    <div class="col-xs-12 center form-group form<?php echo $posicio; ?>imagen_dnia_th" style="border-radius: 15px; background-color: #eeeeee">
                                        <div class="padding5">
                                            <b><?php echo lang("FotoDniDelantera") ?>:</b>
                                        </div>
                                        <div class="padding5">
                                            <?php 
                                                if ($permitirModificacion) {
                                                    ?>
                                                        <input type="button" id="form<?php echo $posicio; ?>btn_lanzar_subida_a" name="form<?php echo $posicio; ?>btn_lanzar_subida_a" value="<?php echo lang("texto_adjuntar_2"); ?>" onclick="lanzar_subida('DNIA', <?php echo $posicio;?>);" class="btnSubmit" style="float:none;"/>
                                                    <?php
                                                }
                                                ?>
                                        </div>
                                        <div class="padding5" style="position:relative;">
                                            <?php 
                                                
                                                $oculto = "";
                                                $dni_delantero = $dato->dni_delantero;
                                                if (!$dni_delantero) {
                                                    $dni_delantero = base_url() . "UploadedFiles/IMG/nodnia150.gif";
                                                    $oculto = "display:none;";
                                                } else { 
                                                    $dni_delantero = $this->soporte_xss->decode($dni_delantero, true);
                                                }
                                                
                                                $extension 	= explode(".", $dni_delantero);
                                                $extension	= $extension[(count($extension)-1)];
                                                $modal = 'data-toggle="modal" data-target="#myModal"';
                                                $url = "#";
                                                $foto = $dni_delantero;
                                                if (strtoupper($extension) == "PDF") {
                                                    $modal = 'target="_blank"';
                                                    $url = $dni_delantero;
                                                    $foto = $img_pdf;
                                                }
                                            ?>
                                            <a href="<?php echo $url; ?>" <?php echo $modal; ?> name="form<?php echo $posicio; ?>a_dnia_th" id="form<?php echo $posicio; ?>a_dnia_th" onclick="mostrar('form<?php echo $posicio; ?>imagen_dnia_th', <?php echo $posicio;?>);">
                                                <img width="150px" src="<?php echo $foto; ?>" name="form<?php echo $posicio; ?>imagen_dnia_th" id="form<?php echo $posicio; ?>imagen_dnia_th" />
                                                <i class="fa fa-spinner fa-spin" style="font-size:24px; display:none;" id="form<?php echo $posicio; ?>spinner_dnia_th" name="form<?php echo $posicio; ?>spinner_dnia_th"></i>
                                                <input type="hidden" name="form<?php echo $posicio; ?>campo_imagen_dnia_th" id="form<?php echo $posicio; ?>campo_imagen_dnia_th" value="<?php echo set_value("form".$posicio."campo_imagen_dnia_th", $dato->dni_delantero); ?>">
                                            </a>
                                            <?php
                                                if ($permitirModificacion) {
                                                    ?><i class="fa fa-remove" id="form<?php echo $posicio; ?>remove_dnia_th" name="form<?php echo $posicio; ?>remove_dnia_th" onclick="lanzar_borrado('form<?php echo $posicio; ?>imagen_dnia_th', 3, <?php echo $posicio;?>);" style="cursor:pointer;position:absolute;top:10px;right:10px;color:white;border-radius:5px;padding:0.5em;background-color:rgba(0,0,0,0.65);<?php echo $oculto; ?>"></i><?php
                                                }
                                            ?>

                                            <div id="vomito" class="uk-width-1-1"></div>
                                        </div>
                                    </div>
                                </div>

                                <div class="col-md-6 col-sm-6 col-xs-12 step2-desc padding5 minH">
                                    <div class="col-xs-12 center form-group form<?php echo $posicio; ?>imagen_dnir_th" style="border-radius: 15px; background-color: #eeeeee">
                                        <div class="padding5">
                                            <b><?php echo lang("FotoDniTrasera") ?>:</b>
                                        </div>
                                        <div class="padding5">
                                            <?php 
                                                if ($permitirModificacion) {
                                                    ?>
                                                        <input type="button" id="form<?php echo $posicio; ?>btn_lanzar_subida_r" name="form<?php echo $posicio; ?>btn_lanzar_subida_r" value="<?php echo lang("texto_adjuntar_2"); ?>" onclick="lanzar_subida('DNIR', <?php echo $posicio;?>);" class="btnSubmit" style="float:none;"/>
                                                    <?php
                                                }
                                                ?>
                                        </div>
                                        <div class="padding5" style="position:relative;">
                                            <?php 
                                                
                                                $oculto = "";
                                                $dni_trasero = $dato->dni_trasero;
                                                if (!$dni_trasero) {
                                                    $dni_trasero = base_url() . "UploadedFiles/IMG/nodnir150.gif";
                                                    $oculto = "display:none;";
                                                } else { 
                                                    $dni_trasero = $this->soporte_xss->decode($dni_trasero, true);
                                                }
                                                $extension 	= explode(".", $dni_trasero);
                                                $extension	= $extension[(count($extension)-1)];
                                                $modal = 'data-toggle="modal" data-target="#myModal"';
                                                $url = "#";
                                                $foto = $dni_trasero;
                                                if (strtoupper($extension) == "PDF") {
                                                    $modal = 'target="_blank"';
                                                    $url = $dni_trasero;
                                                    $foto = $img_pdf;
                                                }
                                            ?>
                                            <a href="<?php echo $url; ?>" <?php echo $modal; ?> name="form<?php echo $posicio; ?>a_dnir_th" id="form<?php echo $posicio; ?>a_dnir_th" onclick="mostrar('form<?php echo $posicio; ?>imagen_dnir_th', <?php echo $posicio;?>);">
                                                <img width="150px" src="<?php echo $foto; ?>" name="form<?php echo $posicio; ?>imagen_dnir_th" id="form<?php echo $posicio; ?>imagen_dnir_th" />
                                                <i class="fa fa-spinner fa-spin" style="font-size:24px; display:none;" id="form<?php echo $posicio; ?>spinner_dnir_th" name="form<?php echo $posicio; ?>spinner_dnir_th"></i>
                                                <input type="hidden" name="form<?php echo $posicio; ?>campo_imagen_dnir_th" id="form<?php echo $posicio; ?>campo_imagen_dnir_th" value="<?php echo set_value("campo_imagen_dnir_th", $dato->dni_trasero) ?>">
                                            </a>
                                            <?php
                                                if ($permitirModificacion) {
                                                    ?><i class="fa fa-remove" id="form<?php echo $posicio; ?>remove_dnir_th" name="form<?php echo $posicio; ?>remove_dnir_th" onclick="lanzar_borrado('form<?php echo $posicio; ?>imagen_dnir_th', 4, <?php echo $posicio;?>);" style="cursor:pointer;position:absolute;top:10px;right:10px;color:white;border-radius:5px;padding:0.5em;background-color:rgba(0,0,0,0.65);<?php echo $oculto; ?>"></i><?php
                                                }
                                            ?>

                                            <div id="vomito" class="uk-width-1-1"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        <?php
/*                      if ($permitirModificacion) {
                            ?></form><?php 
                        } */
                    }
                    ?><input type="hidden" id="pos" name="pos" value="<?php echo $posicio; ?>"/><?php
                
                    if ($permitirModificacion) { 
                        ?>
                            <div class="col-xs-12 step2-desc padding5 padding5 minH alert alert-success hidden" id="alertOk" name="alertOk" style="line-height:45px;">
                                <b><?php echo lang("Validado") ?></b>
                            </div>
                            <div class="col-xs-12 step2-desc padding5 minH" style="margin-bottom:2em;">
                                <input type="button" class="btnSubmit" id="btnSubmit" name="btnSubmit" value="<?php echo lang("ValidarInformacio") ?>" OnClick="validacion(false);" style="float:right;">
                            </div>
                        <?php 
                    } 
                ?>
            </form>
        </div>                    
    </body>
    <?php 
        if ($permitirModificacion) {
            ?>
                <script type="text/javascript">
                    var campoFalla = [];
                    var formFalla = [];
                    var urls = [];
                    <?php
                foreach ($datos as $dato) {
                    ?>urls[urls.length] = "<?php echo $dato->params; ?>";
                    <?php
                }
                    ?>

                    function validacion(enfocar) {
                        window.campoFalla = [];
                        window.formFalla = [];
                        
                        $("#alertOk").removeClass("hidden").addClass("hidden");
                                
                        <?php 
                            for ($x = 1; $x <= $posicio; $x++) {
                                ?>
                                    validart("name", "<?php echo $x; ?>");
                                    validart("sur1", "<?php echo $x; ?>");
                                    validart("sur2", "<?php echo $x; ?>");
                                    validarf("born", "<?php echo $x; ?>");
                                    validart("campo_imagen_dnia_th", "<?php echo $x; ?>");
                                    validart("campo_imagen_dnir_th", "<?php echo $x; ?>");                                        
                                <?php
                            }
                        ?>

                        if (window.campoFalla.length == 0) {
                            if (enfocar) {
                                $("#alertOk").removeClass("hidden");
                                $("#btnSubmit").focus();
                            } else {
                                $("#formreservation").submit();
                            }
                        } else {
                            // console.log("no ha pasado la validacion: "+window.campoFalla);
                            $(".form-group").removeClass("has-error");
                            var focus = "";
                            for (var x = 0; x < window.campoFalla.length; x++) {
                                if (window.campoFalla[x] == "#form"+window.formFalla[x]+"campo_imagen_dnia_th" || window.campoFalla[x] == "#form"+window.formFalla[x]+"campo_imagen_dnir_th") {
                                    $(".form-group"+(window.campoFalla[x].replace("campo_", "").replace("#","."))).addClass("has-error"); 
                                } else {
                                    $(window.campoFalla[x].replace("#",".")).addClass("has-error"); 
                                }
                            }
                            
                            if (window.campoFalla[0] == "#form"+window.formFalla[0]+"campo_imagen_dnia_th") {
                                $("#form"+window.formFalla[0]+"btn_lanzar_subida_a").focus();
                            } else if (window.campoFalla[0] == "#form"+window.formFalla[0]+"campo_imagen_dnir_th") {
                                $("#form"+window.formFalla[0]+"btn_lanzar_subida_r").focus();
                            } else {
                                $(window.campoFalla[0]).focus();
                            }
                            
                            if (!enfocar) 
                                { return false; }
                            else
                                { return true; }
                        }                            
                    }

                    function validart(campo, formulario) {
                        var texto = $("#form"+formulario+campo).val().replace(/^\s*|\s*$/g,"");
                        var ok=false;
                        if (texto.length>0) {
                            ok=true;
                        }           
                        
                        if (!ok) {
                            // console.log("metemos "+campo);
                            window.campoFalla[window.campoFalla.length] =   "#form"+formulario+campo;
                            window.formFalla[window.formFalla.length]   =   formulario;
                        }
                        return ok;
                    }
                    
                    function validarf(campo, formulario){  
                        var cadena = $("#form"+formulario+campo).val();
                        var fecha = new String(cadena); //  Crea un string  
                        var realfecha = new Date()      //  Para sacar la fecha de hoy  
                        //  Cadena Año  
                        var ano= new String(fecha.substring(fecha.lastIndexOf("/")+1,fecha.length))  
                        //  Cadena Mes  
                        var mes= new String(fecha.substring(fecha.indexOf("/")+1,fecha.lastIndexOf("/")))  
                        //  Cadena Día  
                        var dia= new String(fecha.substring(0,fecha.indexOf("/")))  

                        // evitamos que nos metan una fecha sin  /
                        if (fecha.lastIndexOf("/")== -1) {
                            console.log("metemos "+campo);
                            window.campoFalla[window.campoFalla.length] =   "#form"+formulario+campo;
                            window.formFalla[window.formFalla.length]   =   formulario;
                            return false  
                        }
                        
                        // Valido el año  
                        if (isNaN(ano) || ano.length<4 || parseFloat(ano)<1900){  
                            //    alert('Año inválido')  
                            // console.log("metemos "+campo);
                            window.campoFalla[window.campoFalla.length] =   "#form"+formulario+campo;
                            window.formFalla[window.formFalla.length]   =   formulario;
                            return false  
                        }  
                        // Valido el Mes  
                        if (isNaN(mes) || parseFloat(mes)<1 || parseFloat(mes)>12){  
                            // alert('Mes inválido')  
                            // console.log("metemos "+campo);
                            window.campoFalla[window.campoFalla.length] =   "#form"+formulario+campo;
                            window.formFalla[window.formFalla.length]   =   formulario;
                            return false  
                        }  
                        // Valido el Dia  
                        if (isNaN(dia) || parseInt(dia, 10)<1 || parseInt(dia, 10)>31){  
                            // alert('Día inválido')  
                            // console.log("metemos "+campo);
                            window.campoFalla[window.campoFalla.length] =   "#form"+formulario+campo;
                            window.formFalla[window.formFalla.length]   =   formulario;
                            return false  
                        }  
                        if (mes==4 || mes==6 || mes==9 || mes==11 || mes==2) {  
                            if ((mes==2 && dia>28 && (parseFloat(ano)%4!=0)) || (dia>30)) {  
                                //alert('Día inválido')  
                                // console.log("metemos "+campo);
                                window.campoFalla[window.campoFalla.length] =   "#form"+formulario+campo;
                                window.formFalla[window.formFalla.length]   =   formulario;
                                return false  
                            }  
                        }  

                        // para que envie los dato, quitar las  2 lineas siguientes  
                        // alert("Fecha correcta.")  
                        return true
                    }  
                    
                    /*
                        $(function() {
                            $('.calendario').datepicker({
                                dateFormat: 'dd/mm/yy', 
                                showWeek: true,
                                firstDay: 1,
                                numberOfMonths: 1,
                                changeYear: true,
                                regional: $.datepicker.regional['<?php echo strtolower($lang); ?>'], 
                                showOtherMonths: true,
                                selectOtherMonths: true
                            })<?php
                                if ($born == "01/01/1800") {
                                    // echo '.datepicker("setDate", "01/01/' . $z_Anyodesde . '")';
                                } else {
                                    echo '.datepicker("setDate", "' . $born . '")';
                                }
                            ?>;
                        });
                    */

                    function CheckFileName(campo) {
                        var fileName = campo.value
                        if (fileName == "") {
                            //alert("Browse to upload a valid File with png extension");
                            campo.value = null;
                            return false;
                        }
                        else if (
                                fileName.split(".")[1].toUpperCase() == "JPG" ||
                                fileName.split(".")[1].toUpperCase() == "JPEG" ||
                                fileName.split(".")[1].toUpperCase() == "GIF" ||
                                fileName.split(".")[1].toUpperCase() == "PNG"
                                )
                            return true;
                        else {
                            //alert("File with " + fileName.split(".")[1] + " is invalid. Upload a validfile with png extensions");
                            campo.value = null;                        
                            return false;
                        }
                        return true;
                    }
                </script>
            <?php
        }
    ?>

    <div id="myModal" class="modal fade" tabindex="-1" role="dialog">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-body">
                    <img src="" class="img-responsive" id="fotomodal" name="fotomodal">
                </div>
            </div>
        </div>
    </div>
    <?php
    if ($permitirModificacion) {
        ?>
                <script type="text/javascript">
                    function lanzar_subida(tipo, formulario) {
                        $("#formulario").val(formulario);
                        $("#tipo").val(tipo);
                        $("#formIMAGE").attr("action", "<?php echo base_url(); ?>doing/dniupload/"+window.urls[formulario-1]+"/IMG");
                        $("#imagen").click();
                    }

                    function doupload() {
                        $("#formIMAGE").submit();
                    }

                    function mostrar(imagen, formulario) {                        
                        $("#fotomodal").attr("src", $("#"+imagen).attr("src"));
                    }

                    $(document).ready(function (e) {
                        $("#formIMAGE").submit(function(e) {
                            e.preventDefault();
                            var form_data = new FormData();                  
                            var low = 0;
                            var high = 1;
                            var formulario = $("#formulario").val();

                            $(".btnSubmit").attr("disabled", "disabled");

                            var tag = "";
                            switch($("#tipo").val()) {
                                case "DNIA":
                                    tag = "dnia";
                                break;
                                case "DNIR":
                                    tag = "dnir";
                                break;
                            }
                            $("#form"+formulario+"spinner_"+tag+"_th").show(200);
                            
                            for (var x = low; x < high; x++) {
                                var file_data = $('#imagen').prop('files')[x];
                                form_data.append('files', file_data);
                            }
                            $.ajax({
                                url: "<?php echo base_url(); ?>doing/dniupload/"+window.urls[formulario-1]+"/"+$("#tipo").val()+"/true", // Url to which the request is send
                                type: "POST",             // Type of request to be send, called as method
                                data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                                contentType: false,       // The content type used when sending data to the server.
                                cache: false,             // To unable request pages to be cached
                                processData:false,        // To send DOMDocument or non processed data file it is set to false
                                success: function(response) { // A function to be called if request succeeds
                                    console.log("response "+response);
                                    var resultado = response.split(" - ");
                                    var tag = "";
                                    var foto_error="";
                                    switch($("#tipo").val()) {
                                        case "DNIA":
                                            tag = "dnia";
                                            foto_error="nodnia150.gif";
                                        break;
                                        case "DNIR":
                                            tag = "dnir";
                                            foto_error="nodnir150.gif";
                                        break;
                                    }

                                    if (resultado[0]=="Error") {
                                        $("#form"+formulario+"imagen_"+tag+"_th").attr("src", "<?php echo base_url(); ?>UploadedFiles/IMG/"+foto_error).show(200);
                                        var texto = resultado[1].replace(/<[^>]*>?/g, '');
                                        $("#form"+formulario+"imagen_"+tag+"_th").attr("title", texto);
                                        $("#form"+formulario+"spinner_"+tag+"_th").hide(100);
                                        $(".btnSubmit").removeAttr("disabled");
                                        $("#resetear").click();
                                    } else {
                                        extension = response.split(".");
                                        extension = extension[extension.length-1];
                                        var url="";
                                        if (extension.toUpperCase() == "PDF") {
                                            url = response;
                                            response = '<?php echo $img_pdf; ?>';
                                        }
                                        $("#form"+formulario+"campo_imagen_"+tag+"_th").val(response);
                                        $("#form"+formulario+"imagen_"+tag+"_th").attr("src", response).show(200);
                                        $("#form"+formulario+"imagen_"+tag+"_th").attr("title", "");
                                        $("#form"+formulario+"remove_"+tag+"_th").show(200);
                                        $("#form"+formulario+"spinner_"+tag+"_th").hide(100);
                                        $("#form"+formulario+"a_"+tag+"_th").removeAttr("data-toggle").removeAttr("data-target").removeAttr("target");
                                        if (extension.toUpperCase() == "PDF") {
                                            $("#form"+formulario+"a_"+tag+"_th").attr("target","_blank").attr("href", url);

                                        } else {
                                            $("#form"+formulario+"a_"+tag+"_th").attr("data-toggle", "modal").attr("data-target","#myModal").attr("href", "#");
                                        }
                                        $(".btnSubmit").removeAttr("disabled");
                                        $("#resetear").click();
                                    }
                                    validacion();
                                }, 
                                error: function(response) {
                                    $(".btnSubmit").removeAttr("disabled");
                                }
                            });
                        });
                    });
                    
                    function lanzar_borrado(param1, param2, formulario) {
                        var form_data = new FormData();                  
                        form_data.append('param1', param1);
                        form_data.append('param2', param2);
                        form_data.append('formulario', formulario);
                        $.ajax({
                            url: "<?php echo base_url(); ?>doing/dnidelete/"+window.urls[formulario-1]+"/true",
                            type: "POST",             // Type of request to be send, called as method
                            data: form_data, // Data sent to server, a set of key/value pairs (i.e. form fields and values)
                            contentType: false,       // The content type used when sending data to the server.
                            cache: false,             // To unable request pages to be cached
                            processData:false,        // To send DOMDocument or non processed data file it is set to false
                            success: function(response) { // A function to be called if request succeeds
                                switch(param2) {
                                    case 3:
                                        $("#form"+formulario+"campo_imagen_dnia_th").val("");
                                        $("#form"+formulario+"imagen_dnia_th").attr("src", "<?php echo base_url()."UploadedFiles/IMG/nodnia150.gif"; ?>").show(200);
                                        $("#form"+formulario+"remove_dnia_th").hide(100);
                                        $("#form"+formulario+"spinner_dnia_th").hide(100);
                                    break;
                                    case 4:
                                        $("#form"+formulario+"campo_imagen_dnir_th").val("");
                                        $("#form"+formulario+"imagen_dnir_th").attr("src", "<?php echo base_url()."UploadedFiles/IMG/nodnir150.gif"; ?>").show(200);
                                        $("#form"+formulario+"remove_dnir_th").hide(100);
                                        $("#form"+formulario+"spinner_dnir_th").hide(100);
                                    break;
                                }
                                validacion();
                                $("#resetear").click();
                            }
                        });
                    }
    
                </script>
                <form action="<?php echo base_url(); ?>doing/dniupload/<?php echo $param_url; ?>/IMG" method="post" id="formIMAGE" name="formIMAGE"  enctype="multipart/form-data"  class="hidden">
                    <input name="formulario" type="text" id="formulario" value="" />
                    <input name="tipo" type="text" id="tipo" value="" />
                    <input name="resetear" id="resetear" type="reset" />
                    <input name="imagen" type="file" class="FormFill" id="imagen" value="" accept="image/gif,image/jpeg,image/png,image/pjpeg" onchange="javascript:doupload();"/><br />
                </form>
            <?php 
        }
    ?>
</html>