<br/>
<div class="padding5">
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
        <h2><?php echo $numPaso ?> .- <?php echo lang("H_futbol"); ?></h2>                                    
    </div>
</div>
<div class="padding5 box border3">
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
        <b><?php echo lang("exp_futbol") ?></b><br>
        <?php
            if ($readonly) {
                echo ($val_futbol_equipacion == "P" ? lang("Portero") : lang("Jugador"));
            } else {
                ?>
                    <input type="radio" id="futbol_equipacion" name="futbol_equipacion" <?php if ($val_futbol_equipacion == "P") { echo "checked"; }?> value="P" /> <?php echo lang("Portero"); ?><br/>
                    <input type="radio" id="futbol_equipacion" name="futbol_equipacion" <?php if ($val_futbol_equipacion == "J") { echo "checked"; }?> value="J" /> <?php echo lang("Jugador"); ?>
                <?php
            }
        ?>
    </div>
</div>