<script languaje="javascript">
	function controlActivarhipica() {
		if(window.document.formreservation.hipica1[0].checked == true){
			activar = true;
		}else{
			activar = false;
		}
		window.document.formreservation.hipica2[0].disabled = activar;
		window.document.formreservation.hipica2[1].disabled = activar;
		window.document.formreservation.hipica2[2].disabled = activar;
		window.document.formreservation.hipica3[0].disabled = activar;
		window.document.formreservation.hipica3[1].disabled = activar;
		window.document.formreservation.hipica3[2].disabled = activar;
		window.document.formreservation.hipica4[0].disabled = activar;
		window.document.formreservation.hipica4[1].disabled = activar;
		window.document.formreservation.hipica4[2].disabled = activar;

	}
</script>

<br/>
<div class="padding5">
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
        <h2><?php echo $numPaso ?> .- <?php echo lang("titulo_hipica"); ?></h2>                                    
    </div>
</div>
<div class="padding5 box border3">
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
        <h3><?php echo lang("cabecera_hipica"); ?></h3><br>
        <b><?php echo lang("q_1_hipica"); ?></b><br>
        <?php
            if ($readonly) {
                echo lang("hipica1_radio".($val_hipica1+1));
            } else {
                ?>
        		    <input type="radio" id="hipica1" name="hipica1" <?php echo (($val_hipica1 == 0)?"checked":""); ?> value="0" onclick="javascript: controlActivarhipica();"> <?php echo lang("hipica1_radio1") ?><br>
                    <input type="radio" id="hipica1" name="hipica1" <?php echo (($val_hipica1 == 1)?"checked":""); ?> value="1" onclick="javascript: controlActivarhipica();"> <?php echo lang("hipica1_radio2") ?><br>
                    <input type="radio" id="hipica1" name="hipica1" <?php echo (($val_hipica1 == 2)?"checked":""); ?> value="2" onclick="javascript: controlActivarhipica();"> <?php echo lang("hipica1_radio3") ?>
                <?php
            }
        ?>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
		<i><?php echo lang("comentario_hipica") ?></i><br>
		<b><?php echo lang("q_2_hipica") ?></b><br>
        <?php
            if ($readonly) {
                echo lang("hipica2_radio".($val_hipica2+1));
            } else {
                ?>
                    <input type="radio" id="hipica2" name="hipica2" <?php echo (($val_hipica2 == 0)?"checked":""); ?> value="0"> <?php echo lang("hipica2_radio1") ?><br>
                    <input type="radio" id="hipica2" name="hipica2" <?php echo (($val_hipica2 == 1)?"checked":""); ?> value="1"> <?php echo lang("hipica2_radio2") ?><br>
                    <input type="radio" id="hipica2" name="hipica2" <?php echo (($val_hipica2 == 2)?"checked":""); ?> value="2"> <?php echo lang("hipica2_radio3") ?>
                <?php
            }
        ?>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
        <b><?php echo lang("q_3_hipica") ?></b><br>
        <?php
            if ($readonly) {
                echo lang("hipica3_radio".($val_hipica3+1));
            } else {
                ?>
                    <input type="radio" id="hipica3" name="hipica3" <?php echo (($val_hipica3 == 0)?"checked":""); ?> value="0"> <?php echo lang("hipica3_radio1") ?><br>
                    <input type="radio" id="hipica3" name="hipica3" <?php echo (($val_hipica3 == 1)?"checked":""); ?> value="1"> <?php echo lang("hipica3_radio2") ?><br>
                    <input type="radio" id="hipica3" name="hipica3" <?php echo (($val_hipica3 == 2)?"checked":""); ?> value="2"> <?php echo lang("hipica3_radio3") ?>
                <?php
            }
        ?>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
        <b><?php echo lang("q_4_hipica") ?></b><br>
        <?php
            if ($readonly) {
                echo lang("hipica4_radio".($val_hipica4+1));
            } else {
                ?>
                    <input type="radio" id="hipica4" name="hipica4" <?php echo (($val_hipica4 == 0)?"checked":""); ?> value="0"> <?php echo lang("hipica4_radio1") ?><br>
                    <input type="radio" id="hipica4" name="hipica4" <?php echo (($val_hipica4 == 1)?"checked":""); ?> value="1"> <?php echo lang("hipica4_radio2") ?><br>
                    <input type="radio" id="hipica4" name="hipica4" <?php echo (($val_hipica4 == 2)?"checked":""); ?> value="2"> <?php echo lang("hipica4_radio3") ?>
                <?php
            }
        ?>
    </div>
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
		<i><?php echo lang("comentario_observaciones_hipica") ?></i><br>
		<b><?php echo lang("observaciones_hipica") ?></b><br>
		<textarea cols="40" <?php echo ($readonly? 'disabled="disabled"': ""); ?> rows="5" id="observaciones_hipica" class="form-control" name="observaciones_hipica"><?php echo $val_observaciones_hipica ?></textarea>
	</div>
</div>
