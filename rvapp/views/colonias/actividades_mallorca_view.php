<br/>
<div class="padding5">
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
        <h2><?php echo $numPaso ?> .- <?php echo lang("H_Mallorca"); ?></h2>
    </div>
</div>
<div class="padding5 box border3">
    <div class="col-md-12 col-sm-12 col-xs-12 step2-desc padding5">
        <?php echo lang("exp_mallorca"); ?><br />
        <?php 
            if ($readonly) {
                echo $val_mallorca_dni;
            } else {
                ?><input type="text" name="mallorca_dni" id="mallorca_dni" value="<?php echo $val_mallorca_dni ?>" class="input" /><?php
            }
        ?>
	</div>
</div>