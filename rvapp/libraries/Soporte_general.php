<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

    class Soporte_general {

        /* funcion para la inversion de formatos de fechas */
        function cambia_fecha($fecha, $separador_original, $separador_definitivo) {
            $retorno = "";
            if ($fecha != "") {
                $valor = explode($separador_original, $fecha);
                $limpiamos_resto = explode(" ", $valor[2]);
                $valor[2] = $limpiamos_resto[0];
                if ($valor[0] + $valor[1] + $valor[2]) {
                    $retorno = $valor[2].$separador_definitivo.$valor[1].$separador_definitivo.$valor[0];
                } 
            } 
            return $retorno;
        }
    }
?>