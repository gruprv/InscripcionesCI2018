<?php
	defined('BASEPATH') OR exit('No direct script access allowed');

	class Test extends CI_Controller {

		protected $devlog = true;

		/* propiedades */
			protected $ni = 0;	// id de inscripcion
			protected $tr = 1;	// tipo de reserva  	2 Idiomas 	/ 1 colonias 	/ 4 Taf Hotelera
			protected $ip = 0;	// id de people
			protected $ic = 0;	// clave de control para asegurar que el registro que buscamos es el que toca
			protected $param_url = "";	// url encriptada original

			protected $idioma = 'es'; 	// idioma test
			protected $forzar = false; 	// usamos este parametro para forzar el acceso en edicion incluso cuando ha sido finalizada.
		/* FIN - propiedades */
		
		public function __construct() {
			parent::__construct();
		}

		public function index() {
			$this->do();
		}
		
		public function do($params = "") {
			$this->soporte_xss->inicializar();
			if ($params!="") {
				$cadena = $this->soporte_xss->desencriptar_params($params);
				echo "<pre name=\"Main\">Decoded => $cadena == $params</pre>"; 
			} else {
				$cadena =  "me gusta el polo pistacho";
				$params = $this->soporte_xss->encriptar_params($cadena);
				echo "<pre name=\"Main\">Encoded => $cadena == $params</pre>"; 
			}
		}
	}
?>
