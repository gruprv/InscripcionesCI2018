<?php
    defined('BASEPATH') OR exit('No direct script access allowed');

    class Soporte extends CI_Controller {

        public function __construct() {
            parent::__construct();
        }

        public function index() {
            echo "Soporte!";
        }

        public function encode($modo) {

            $this->soporte_xss->inicializar($modo);
            echo $this->soporte_xss->encode("Es muy grande?                I   N   M   E   N   S   A   !");            
        }
    }
?>