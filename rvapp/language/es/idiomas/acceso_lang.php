<?php
	$lang["TitolWeb"]          = "Acceso a la ficha de inscripción.";
	$lang["Titol"]          = "Acceso a la ficha de inscripción.";
	$lang["atencion_no_reserva"]    = "	<center>
											<h1>
												Información Importante:<br/><br/>
												Está a punto de entrar en la Ficha de Inscripción<br/> 
												de Rosa dels Vents Idiomas.
											</h1>
										</center><br/><br/>
										<div style='text-align: left;'>
											En esta Ficha de Inscripción podrá ampliar la información sobre su hij@ y así podremos ofrecerle un mejor servicio.<br/><br/>
											Por favor no olvide detallar los datos sanitarios, medicamentos, alergias, etc. <br/><br/>
											Una vez completada la Ficha de Inscripción, no podrá volver a acceder. Si desea ampliar o modificar algún dato: nueva enfermedad, uso de medicamentos, cualquier otra información que usted considere importante, por favor envíenos un email a <a href='mailto://info@rosadelsventsidiomas.es'>info@rosadelsventsidiomas.es</a>, necesitaremos:<br/>
											<ul>
												<li>Número de la reserva</li>
												<li>Nombre del participante</li>
												<li>Fechas de entrada y salida</li>
												<li>Información adicional que desee incluir en la Ficha de Inscripción</li>
												</ul>
											Esta Ficha de Inscripción no da derecho alguno a plaza si no se han realizado todos los pagos correspondientes.<br/><br/>
										</div>";
	$lang["atencion_no_reserva2"]   = "";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Referencia";
	$lang["centro"] = "Centro";
	$lang["programa"] ="Programa";
	$lang["Dni"] = "DNI";
	$lang["formatDni"] = "Ej: 00000000A";
	$lang["BtnSubmit"] = "Acceder al sistema";
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";
	$lang["NoFound"] = "No se ha encontrado ninguna reserva con estos datos identificacivos.<br />Por favor intentelo de nuevo pasado 24 horas<br />(es posible que aun no haya sido procesada por el sistema).";
	$lang["titulo_acceso"] = "Acceso a la inscripción";
	$lang["Texto_aplazado1"] = "El formulario de inscripción a los cursos de idiomas estará disponible a partir de la fecha: ";
	$lang["Texto_aplazado2"] = ", por favor vuelva pasada esta fecha para completar el proceso de inscripción de sus hijos/as";
	$lang["finaliza_correctamente"] = "Su inscripcion ha finalizado.";
	$lang["finaliza_despedida"] = "Gracias por su atención, y Muchas gracias por confiar en Rosa dels Vents.";
	$lang["titulo_info"] = "Importante.";
	$lang["test_nom"] = "Nombre y apellidos del participante";
	$lang["finaliza_test_correctamente"] = "Ha finalizado la prueba.";
	$lang["Titol_test"] = "Acceso a la prueba de nivel de inglés.";
	$lang["MensajeFinalizado"] = "La ficha de inscripción ha sido completada correctamente.";
	$lang["MensajeFinalizadoTest"] = "La prueba de nivel ha sido completada correctamente.";
	$lang["MensajeNoExiste"] = "Atención, la Ficha de Inscripción / Prueba de Nivel a la que intenta acceder ha dejado de existir. Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
	// $lang["MensajeYaFinalizado"] = "Atención, la Ficha de Inscripción / Prueba de Nivel a la que intenta acceder ya ha sido finalizada con anterioridad. Por favor, si necesita hacer alguna modificación llame al Servicio de Atención al Cliente de Rosa dels Vents Idiomas al teléfono <b>935.036.035</b> o envíe un email a <a href='mailto:info@rosadelsventsidiomas.es'><b>info@rosadelsventsidiomas.es</b></a>.";
	$lang["MensajeYaFinalizado"] = "La Ficha de Inscripción a la que intenta acceder ya ha sido finalizada anteriormente.<br/><br/>	Si necesita hacer alguna modificación, le agradeceremos nos envié un email al Servicio de Atención al cliente de Rosa dels Vents a: <a href='mailto:info@rosadelsventsidiomas.es'><b>info@rosadelsventsidiomas.es</b></a>.";
	$lang["MensajeTestOutDated"] = "Atención, ya no es posible acceder ni a la Ficha de Inscripción ni al Test de Nivel.<br/><br/>El período para realizar estos tramites finaliza 30 días después de formalizar la reserva.<br/><br/>Los participantes que no hayan podido hacer el test de nivel on-line lo harán en la escuela el primer día.";
	
	$lang["MensajeDe1En1"] = "Atención: Ya tiene una <u>Ficha de inscripción</u> o <u>Prueba de nivel</u> abierta en este navegador. <br/><br/>Por favor, finalicela antes de continuar con las siguientes.<br/><br/>Si la ha cerrado y continua saliendo este mensaje, por favor, cierre este navegador y vuelva a entrar.<br/><br/>Disculpe las molestias.";
?>