<?php
    // $lang[""] ="";

    $lang["TitolWeb"] = "Test de nivel de Inglés";
    $lang["Titol"] = "Test de nivel de Inglés";
    $lang["NReserva"] = "Número Reserva";
    $lang["Localitzador"] = "Localizador";
    $lang["Dni"] = "DNI";
    $lang["Participant"] = "Participante";
    
    $lang["H_Titulo"] = "Level Test";
    $lang["H3_Enunciado"] = "Select the correct answer. Only ONE answer is correct";
    
    $lang["boton"] = "Enviar el Test";
    $lang["AlertaEnviament"] = "Esta seguro que quiere enviar el Test? una vez enviada no podrá volver a entrar al Test.";
    $lang["AlertaFaltanCampos"] = "Para poder continuar has de responder todas las preguntas";
    
    $lang["True"] = "Sí";
    $lang["False"] = "No";
    $lang["val_True"] = 1;
    $lang["val_False"] = 0;
    
    $lang["Idioma"] = "Idioma";
    $lang["Idioma1"] = "Catalán";
    $lang["Idioma2"] = "Castellano";
    
    $lang["H_Info"] = "Información adicional";

    $lang["NSNC"] = "Deseo pertenecer al nivel Básico";
    $lang["MensajeNoExiste"] = "Atención, la ficha a la que intenta acceder ha dejado de existir.<br/><br/>Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
    
    $lang["AciertosNSNC"] = "<br/><br/>He decidido pertenecer al <b>nivel Básico</b>";
    $lang["MensajeFinalizadoTest"] = "La prueba de nivel ha sido completada correctamente.";
    $lang["MensajeTestOutDated"] = "Atenció, ja no és possible accedir ni al Test de Nivell ni a la Fitxa d'Inscripció.<br/><br/>El període per a realitzar aquests tramits finalitza 30 dies despres de formalitzar la reserva.<br/><br/>Els participants que no hagin pogut fer el test de nivell on-line el faran a l'escola el primer dia.";
    $lang["NoFound"] = "No se ha encontrado ninguna reserva con estos datos identificativos.<br />Por favor inténtelo de nuevo pasado 24 horas<br />(es posible que aun no haya sido procesada por el sistema).";
    // $lang["AlreadyDone"] = "Prueba de nivel realizada con anterioridad.";
    $lang["AlreadyDone"] = "La Prueba de inglés a la que intenta acceder ya ha sido finalizada anteriormente.<br/><br/>Si necesita hacer alguna modificación, le agradeceremos nos envié un email al Servicio de Atención al cliente de Rosa dels Vents a: <a href='mailto:info@rosadelsventsidiomas.es'><b>info@rosadelsventsidiomas.es</b></a>.";
    
    $lang["Splitter"]   ="|@|";
    $lang["npreguntas"] = 50;
    $lang["Aciertos"] = "<br/><br/>¡Ha acertado <b>{respuestas}/".$lang["npreguntas"]." respuestas!</b>";
    $lang["Info_Descripcion"] = "Este test tiene como finalidad valorar el nivel del estudiante y poder  colocarlo en el nivel que le corresponda junto con estudiantes de nivel similar, por lo que se ruega hagan la prueba sin ayuda y sin diccionario.<br/><br/>El test consta de ".$lang["npreguntas"]." preguntas, para poder considerar el test como valido todas las  preguntas tienen que tener respuesta.<br /><br /><b style='color:firebrick;'>ATENCION!! Una vez finalizado el test no se puede repetir.</b><br /><br />En el caso de no querer contestar a las preguntas y quedar inscritos en el nivel Básico cliclar aquí: ";
    $lang["Textaxo"]  = "She ___ like pizza.".$lang["Splitter"].
                        "don’t".$lang["Splitter"].
                        "doesn’t".$lang["Splitter"].
                        "not".$lang["Splitter"].
                        "never".$lang["Splitter"].

                        "What time ___ the concert start tomorrow?".$lang["Splitter"].
                        "do".$lang["Splitter"].
                        "did".$lang["Splitter"].
                        "does".$lang["Splitter"].
                        "done".$lang["Splitter"].

                        "I ___ a new phone last week.".$lang["Splitter"].
                        "buy".$lang["Splitter"].
                        "buys".$lang["Splitter"].
                        "bought".$lang["Splitter"].
                        "buying".$lang["Splitter"].

                        "She felt ___ when she heard the bad news.".$lang["Splitter"].
                        "happy".$lang["Splitter"].
                        "excited".$lang["Splitter"].
                        "sad".$lang["Splitter"].
                        "fantastic".$lang["Splitter"].

                        "We ___ dinner together last night, when the phone rang.".$lang["Splitter"].
                        "had eaten".$lang["Splitter"].
                        "ate".$lang["Splitter"].
                        "had been eating".$lang["Splitter"].
                        "were eating".$lang["Splitter"].

                        "If I ___ more time, I would spend more time studying.".$lang["Splitter"].
                        "have".$lang["Splitter"].
                        "has".$lang["Splitter"].
                        "had".$lang["Splitter"].
                        "will have".$lang["Splitter"].

                        "She has ___ a new dress for the wedding.".$lang["Splitter"].
                        "buy".$lang["Splitter"].
                        "buys".$lang["Splitter"].
                        "bought".$lang["Splitter"].
                        "buying".$lang["Splitter"].

                        "I ___ swimming every Thursday when I was younger.".$lang["Splitter"].
                        "go".$lang["Splitter"].
                        "went".$lang["Splitter"].
                        "going".$lang["Splitter"].
                        "goes".$lang["Splitter"].

                        "By 3 PM tomorrow, the meeting ___.".$lang["Splitter"].
                        "will start".$lang["Splitter"].
                        "starts".$lang["Splitter"].
                        "is starting".$lang["Splitter"].
                        "will have started".$lang["Splitter"].

                        "My brother is taller ___ me.".$lang["Splitter"].
                        "that".$lang["Splitter"].
                        "than".$lang["Splitter"].
                        "as".$lang["Splitter"].
                        "to".$lang["Splitter"].

                        "They ___ breakfast when the phone rang.".$lang["Splitter"].
                        "were having".$lang["Splitter"].
                        "had".$lang["Splitter"].
                        "have".$lang["Splitter"].
                        "having".$lang["Splitter"].

                        "I have ___ been to this restaurant before.".$lang["Splitter"].
                        "neither".$lang["Splitter"].
                        "not".$lang["Splitter"].
                        "no".$lang["Splitter"].
                        "none".$lang["Splitter"].

                        "The train is moving too ___.".$lang["Splitter"].
                        "speed".$lang["Splitter"].
                        "slower".$lang["Splitter"].
                        "slowest".$lang["Splitter"].
                        "slowly".$lang["Splitter"].

                        "This lion is ___ bigger than the one at the zoo.".$lang["Splitter"].
                        "most ".$lang["Splitter"].
                        "much".$lang["Splitter"].
                        "many".$lang["Splitter"].
                        "very".$lang["Splitter"].

                        "I’m looking forward ___ going on holiday for Easter.".$lang["Splitter"].
                        "at".$lang["Splitter"].
                        "to".$lang["Splitter"].
                        "for".$lang["Splitter"].
                        "with".$lang["Splitter"].

                        "She ___ arrive to the party later tonight, I’m not sure.".$lang["Splitter"].
                        "will".$lang["Splitter"].
                        "should".$lang["Splitter"].
                        "must".$lang["Splitter"].
                        "might".$lang["Splitter"].

                        "They ___ for a new house for two months when they finally found the perfect one last week.".$lang["Splitter"].
                        "search".$lang["Splitter"].
                        "searched".$lang["Splitter"].
                        "have been searching".$lang["Splitter"].
                        "had been searching".$lang["Splitter"].

                        "The people ___ live in the flat next to me are really nice.".$lang["Splitter"].
                        "who".$lang["Splitter"].
                        "that".$lang["Splitter"].
                        "where".$lang["Splitter"].
                        "whom".$lang["Splitter"].

                        "A new shopping centre ___ built in London in by 2030.".$lang["Splitter"].
                        "will have been".$lang["Splitter"].
                        "was being".$lang["Splitter"].
                        "has been".$lang["Splitter"].
                        "had been".$lang["Splitter"].

                        "He wishes he___ more money to go out with his friends.".$lang["Splitter"].
                        "has".$lang["Splitter"].
                        "had".$lang["Splitter"].
                        "have ".$lang["Splitter"].
                        "having".$lang["Splitter"].

                        "If she ___ harder, she would pass the exam.".$lang["Splitter"].
                        "study".$lang["Splitter"].
                        "studies".$lang["Splitter"].
                        "studied".$lang["Splitter"].
                        "studying".$lang["Splitter"].

                        "There aren’t___ cookies left, it’s totally empty!".$lang["Splitter"].
                        "some".$lang["Splitter"].
                        "many".$lang["Splitter"].
                        "enough".$lang["Splitter"].
                        "any".$lang["Splitter"].

                        "They ___ for a new house for two months.".$lang["Splitter"].
                        "search".$lang["Splitter"].
                        "searched".$lang["Splitter"].
                        "have been searching".$lang["Splitter"].
                        "are searching".$lang["Splitter"].

                        "I ___ tea every morning.".$lang["Splitter"].
                        "drink".$lang["Splitter"].
                        "drank".$lang["Splitter"].
                        "drinks".$lang["Splitter"].
                        "drinking".$lang["Splitter"].

                        "She ___ seen her cousin in years.".$lang["Splitter"].
                        "hasn’t".$lang["Splitter"].
                        "didn’t".$lang["Splitter"].
                        "not".$lang["Splitter"].
                        "isn’t".$lang["Splitter"].

                        "She gave me ___ orange from the basket, but I prefer apples.".$lang["Splitter"].
                        "the".$lang["Splitter"].
                        "a".$lang["Splitter"].
                        "an".$lang["Splitter"].
                        "no article".$lang["Splitter"].

                        "My classroom is ___ clean, it's spotless!".$lang["Splitter"].
                        "extremely".$lang["Splitter"].
                        "barely".$lang["Splitter"].
                        "somewhat".$lang["Splitter"].
                        "reasonably".$lang["Splitter"].

                        "She lost her glasses and her workbook, but I haven't seen ____ of them.".$lang["Splitter"].
                        "either".$lang["Splitter"].
                        "neither".$lang["Splitter"].
                        "both".$lang["Splitter"].
                        "any".$lang["Splitter"].

                        "She ___ me to go to the cinema yesterday.".$lang["Splitter"].
                        "invited".$lang["Splitter"].
                        "invites".$lang["Splitter"].
                        "inviting".$lang["Splitter"].
                        "invite".$lang["Splitter"].

                        "You should ___ a doctor if you feel unwell.".$lang["Splitter"].
                        "see".$lang["Splitter"].
                        "saw".$lang["Splitter"].
                        "seeing".$lang["Splitter"].
                        "sees".$lang["Splitter"].

                        "The room ___ cleaned every day.".$lang["Splitter"].
                        "is".$lang["Splitter"].
                        "were".$lang["Splitter"].
                        "has".$lang["Splitter"].
                        "was".$lang["Splitter"].

                        "We ___ waiting for the bus for over an hour.".$lang["Splitter"].
                        "have been".$lang["Splitter"].
                        "are".$lang["Splitter"].
                        "has been".$lang["Splitter"].
                        "were".$lang["Splitter"].

                        "She suggested ___ Manchester instead of Liverpool.".$lang["Splitter"].
                        "visit".$lang["Splitter"].
                        "visiting".$lang["Splitter"].
                        "visits".$lang["Splitter"].
                        "visited".$lang["Splitter"].

                        "If I had more time, I ___ help you with your homework.".$lang["Splitter"].
                        "will".$lang["Splitter"].
                        "would".$lang["Splitter"].
                        "have".$lang["Splitter"].
                        "can".$lang["Splitter"].

                        "The teacher told us ___ our homework by Friday.".$lang["Splitter"].
                        "finish".$lang["Splitter"].
                        "finishing".$lang["Splitter"].
                        "to finish".$lang["Splitter"].
                        "finishes".$lang["Splitter"].

                        "When it ___ raining, we can go outside.".$lang["Splitter"].
                        "stops".$lang["Splitter"].
                        "stop".$lang["Splitter"].
                        "stopped".$lang["Splitter"].
                        "stopping".$lang["Splitter"].

                        "I’m really looking forward to ___ on holiday this summer.".$lang["Splitter"].
                        "making".$lang["Splitter"].
                        "doing".$lang["Splitter"].
                        "going".$lang["Splitter"].
                        "taking".$lang["Splitter"].

                        "They ___ to a new city next month.".$lang["Splitter"].
                        "move".$lang["Splitter"].
                        "moved".$lang["Splitter"].
                        "moves".$lang["Splitter"].
                        "are moving".$lang["Splitter"].

                        "The cake ___ by my sister’s friend for my birthday.".$lang["Splitter"].
                        "is baked".$lang["Splitter"].
                        "was baked".$lang["Splitter"].
                        "baked".$lang["Splitter"].
                        "bakes".$lang["Splitter"].

                        "My friend ___ this book to me last year, it’s really good.".$lang["Splitter"].
                        "recommended".$lang["Splitter"].
                        "recommends".$lang["Splitter"].
                        "recommending".$lang["Splitter"].
                        "recommend".$lang["Splitter"].

                        "He wishes he ___ more money to travel.".$lang["Splitter"].
                        "has".$lang["Splitter"].
                        "had".$lang["Splitter"].
                        "have".$lang["Splitter"].
                        "having".$lang["Splitter"].

                        "I think it’s time to ___ and go home.".$lang["Splitter"].
                        "call it a day".$lang["Splitter"].
                        "break the ice".$lang["Splitter"].
                        "get the ball rolling".$lang["Splitter"].
                        "give it a go".$lang["Splitter"].

                        "By the end of the week, we ___ cleaned the entire house.".$lang["Splitter"].
                        "will clean".$lang["Splitter"].
                        "will have cleaned".$lang["Splitter"].
                        "clean".$lang["Splitter"].
                        "cleaning".$lang["Splitter"].

                        "She asked me where I ___ from.".$lang["Splitter"].
                        "come".$lang["Splitter"].
                        "came".$lang["Splitter"].
                        "coming".$lang["Splitter"].
                        "comes".$lang["Splitter"].

                        "The article ___ translated into many languages.".$lang["Splitter"].
                        "has been".$lang["Splitter"].
                        "is being".$lang["Splitter"].
                        "is".$lang["Splitter"].
                        "was".$lang["Splitter"].

                        "She loves ___ TV all day.".$lang["Splitter"].
                        "watch".$lang["Splitter"].
                        "watched".$lang["Splitter"].
                        "watches".$lang["Splitter"].
                        "watching".$lang["Splitter"].

                        "They wanted to eat some pizza, __ they didn’t have any on the menu.".$lang["Splitter"].
                        "although".$lang["Splitter"].
                        "because".$lang["Splitter"].
                        "but".$lang["Splitter"].
                        "however".$lang["Splitter"].

                        "I ___ this movie before.".$lang["Splitter"].
                        "didn’t see".$lang["Splitter"].
                        "haven’t seen".$lang["Splitter"].
                        "haven’t saw".$lang["Splitter"].
                        "don’t see".$lang["Splitter"].

                        "The job requires someone who ___ speak French fluently.".$lang["Splitter"].
                        "should".$lang["Splitter"].
                        "could".$lang["Splitter"].
                        "may".$lang["Splitter"].
                        "can".$lang["Splitter"].

                        "If he ___ more carefully, he wouldn’t make mistakes.".$lang["Splitter"].
                        "works".$lang["Splitter"].
                        "work".$lang["Splitter"].
                        "worked".$lang["Splitter"].
                        "working".$lang["Splitter"];
?>
?>