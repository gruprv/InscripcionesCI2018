<?php
    // $lang[""] ="";

    $lang["TitolWeb"] = "Encuesta de validación de nivel de Inglés";
    $lang["Titol"] = "Encuesta de validación<br/>de nivel de Inglés";
    $lang["NReserva"] = "Número Reserva";
    $lang["Localitzador"] = "Localizador";
    $lang["Dni"] = "DNI";
    $lang["Participant"] = "Participante";
    
    $lang["H_Titulo"] = "Level Test";
    $lang["H3_Enunciado"] = "Select the correct answer. Only ONE answer is correct";
    
    $lang["boton"] = "Enviar la Encuesta";
    $lang["AlertaEnviament"] = "Esta seguro que quiere enviar la encuesta? una vez enviada no podrá volver a entrar a la encuesta.";
    $lang["AlertaFaltanCampos"] = "Para poder continuar has de responder todas las preguntas";
    $lang["PruebaNoRealizada"] = "Prueba de nivel No Realizada";

    $lang["True"] = "Sí";
    $lang["False"] = "No";
    $lang["val_True"] = 1;
    $lang["val_False"] = 0;
    
    $lang["Idioma"] = "Idioma";
    $lang["Idioma1"] = "Catalán";
    $lang["Idioma2"] = "Castellano";
    
    $lang["H_Info"] = "Información adicional";
    $lang["Info_Descripcion"] = "A continuación podréis responder a las cuestiones de la siguiente prueba de nivel. El motivo del cuestionario es separar  a los participantes en tres niveles de inglés: Básico, Intermedio y Alto. <br /><br /><b style='color:firebrick;'>ATENCI&Oacute;N: El test no se puede repetir, por favor, meditad bien las respuestas.</b><br /><br />En el caso de que no desee contestar las preguntas y quedar inscrito en el nivel básico clicar aquí: ";
    $lang["NSNC"] = "Deseo pertenecer al nivel Básico";
    $lang["MensajeNoExiste"] = "Atención, la ficha a la que intenta acceder ha dejado de existir.<br/><br/>Vuelva a acceder a su reserva para continuar con el proceso de inscripción.";
    
    $lang["AciertosNSNC"] = "<br/><br/>He decidido pertenecer al <b>nivel Básico</b>";
    $lang["MensajeFinalizadoTest"] = "La prueba de nivel ha sido completada correctamente.";
    $lang["MensajeTestOutDated"] = "Atención, ya no es posible acceder al test de nivel.<br/>El período para realizar el test de nivel es desde el día de la<br/>reserva hasta 15 días antes del inicio del curso.<br/>Los participantes que no hayan podido hacer el test de nivel on-line<br/>lo harán en la escuela el primer día.";
    $lang["NoFound"] = "No se ha encontrado ninguna reserva con estos datos identificativos.<br />Por favor inténtelo de nuevo pasado 24 horas<br />(es posible que aun no haya sido procesada por el sistema).";
    $lang["AlreadyDone"] = "Prueba de nivel realizada con anterioridad.";

    $lang["Splitter"]   ="|@|";
    $lang["npreguntas"] = 20;
    $lang["Aciertos"] = "<br/><br/>¡Ha acertado <b>{respuestas}/".$lang["npreguntas"]." respuestas!</b>";
    $lang["Textaxo"]    =   "How old are you?".$lang["Splitter"].
                            "I have 9 years old".$lang["Splitter"].
                            "I am 9 years old".$lang["Splitter"].
                            "I have 9 years".$lang["Splitter"].
                            
                            "_______ are you from?".$lang["Splitter"].
                            "Who".$lang["Splitter"].
                            "What".$lang["Splitter"].
                            "Where".$lang["Splitter"].
                            
                            "Where is the book? – The book is _______ the table".$lang["Splitter"].
                            "At".$lang["Splitter"].
                            "On".$lang["Splitter"].
                            "In".$lang["Splitter"].
                            
                            "How long have you been living in your town?".$lang["Splitter"].
                            "For 7 years".$lang["Splitter"].
                            "Since 7 years".$lang["Splitter"].
                            "At 7 years".$lang["Splitter"].
                            
                            "She plays basketball _______ Mondays.".$lang["Splitter"].
                            "In".$lang["Splitter"].
                            "On".$lang["Splitter"].
                            "At".$lang["Splitter"].
                            
                            "We haven’t got _______ food in the fridge.".$lang["Splitter"].
                            "Some".$lang["Splitter"].
                            "Very".$lang["Splitter"].
                            "Any".$lang["Splitter"].
                            
                            "They are going to France with _______ son".$lang["Splitter"].
                            "There".$lang["Splitter"].
                            "Here".$lang["Splitter"].
                            "Their".$lang["Splitter"].
                            
                            "I _______ to the supermarket at the moment.".$lang["Splitter"].
                            "Am going ".$lang["Splitter"].
                            "Go ".$lang["Splitter"].
                            "Went".$lang["Splitter"].
                            
                            "My suitcase is heavy, but yours is _______".$lang["Splitter"].
                            "More heavy".$lang["Splitter"].
                            "Heavier".$lang["Splitter"].
                            "Heavyer".$lang["Splitter"].
                            
                            "What time _______ to school every day?".$lang["Splitter"].
                            "Do you go".$lang["Splitter"].
                            "You go".$lang["Splitter"].
                            "Are you going".$lang["Splitter"].
                            
                            "Where _______ you go yesterday?".$lang["Splitter"].
                            "Do".$lang["Splitter"].
                            "Are".$lang["Splitter"].
                            "Did".$lang["Splitter"].
                            
                            "What _______ he like to do?".$lang["Splitter"].
                            "Does".$lang["Splitter"].
                            "Do".$lang["Splitter"].
                            "Has".$lang["Splitter"].
                            
                            "Where is Susan? - She’s _______ holiday.".$lang["Splitter"].
                            "In".$lang["Splitter"].
                            "On".$lang["Splitter"].
                            "By".$lang["Splitter"].
                            
                            "What _______ you like to drink?".$lang["Splitter"].
                            "Will".$lang["Splitter"].
                            "Could".$lang["Splitter"].
                            "Would".$lang["Splitter"].
                            
                            "That car is _______ than mine.".$lang["Splitter"].
                            "More fast ".$lang["Splitter"].
                            "Faster".$lang["Splitter"].
                            "Most fast".$lang["Splitter"].
                            
                            "Anne _______ in an office.".$lang["Splitter"].
                            "Working".$lang["Splitter"].
                            "Work".$lang["Splitter"].
                            "Works".$lang["Splitter"].
                            
                            "It’s _______ expensive apple.".$lang["Splitter"].
                            "One".$lang["Splitter"].
                            "A".$lang["Splitter"].
                            "An".$lang["Splitter"].
                            
                            "_______ cat is black".$lang["Splitter"].
                            "Her".$lang["Splitter"].
                            "They".$lang["Splitter"].
                            "Him".$lang["Splitter"].
                            
                            "_______ books are those?".$lang["Splitter"].
                            "Whose".$lang["Splitter"].
                            "Why".$lang["Splitter"].
                            "Who".$lang["Splitter"].
                            
                            "What are you waiting _______?".$lang["Splitter"].
                            "At ".$lang["Splitter"].
                            "On".$lang["Splitter"].
                            "For".$lang["Splitter"];
?>