<?php

	$lang["AclaracionFoto"] ="* La foto no serà visible fins que no hagi Salvat les dades.";
	$lang["texto_caja_email"]= "Posi aquí el seu email si desitja rebre un correu electrònic amb la confirmació de la inscripció.";
	$lang["confirmacionBorrado"] = "Esta segur que vol esborrar aquesta fitxa?";
	$lang["AlertaValidacio"]="Tots els camps son obligatoris";
	$lang["AlertaFinNivell"]="Encara te fitxes de participants que no han realitzat la Prova de Nivell d'Angles.";
	$lang["AlertaFinalitzacio"] ="Esta segur que vol finalitzar la inscripció de la seva reserva?";

	// formateig de dates.
	$lang["enero"] = "Gener"; 
	$lang["febrero"] = "Febrer";
	$lang["marzo"] = "Març";
	$lang["abril"] = "Abril";
	$lang["mayo"] = "Maig";
	$lang["junio"] = "Juny";
	$lang["julio"] = "Juliol";
	$lang["agosto"] = "Agost";
	$lang["septiembre"] = "Setembre";
	$lang["octubre"] = "Octubre";
	$lang["noviembre"] = "Novembre";
	$lang["diciembre"] = "Desembre";
		
	$lang["a"] = "a";
	$lang["lunes"] = "Dilluns";
	$lang["martes"] = "Dimarts";
	$lang["miercoles"] = "Dimecres";
	$lang["jueves"] = "Dijous";
	$lang["viernes"] = "Divendres";
	$lang["sabado"] = "Dissabte";
	$lang["domingo"] = "Diumenge";
	$lang["de"] = "de";
	$lang["del"] = "del";
		
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";

	$lang["Enviado"] = "Enviat";
	$lang["Pendiente"] = "Pendent d'enviar";
	$lang["Estatus"] = "Estat";
	$lang["EnquestaPendent"] = "Pendent de la enquesta";
		
		// resta de la plana.
	$lang["TitolWeb"] = "Sistema d’inscripció a Colònies";
	$lang["Titol"] = "Full d'inscripció";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Localitzador";
	$lang["Dni"] = "DNI";
	$lang["centro"] = "Centre";
	$lang["programa"] = "Programa";

	$lang["True"] = "Sí";
	$lang["False"] = "No";
	$lang["val_True"] = 1;
	$lang["val_False"] = 0;
		
	$lang["H_Resumen"] = "Resum de la Inscripció";

	$lang["H_SeccioDPersonals"] = "Dades personals del participant:";
	$lang["NomComplet"] = "Nom complert del participant NEN / NENA";
	$lang["Telefon"] = "Telèfon del responsable";
	$lang["Chico_chica"] = "Sexe del participant";
	$lang["chico"] = "Nen";
	$lang["chica"] = "Nena";
	$lang["Adreça"] = "Adreça";
	$lang["Poblacio"] = "Població";
	$lang["CP"] = "CP";
	$lang["DataNaixement"] = "Data de naixement (DD/MM/AAAA)";
	$lang["AltresTelefons"] = "Altres telèfons en cas d’urgència";
	$lang["Foto"] = "Foto";
		
	$lang["H_InformacioSanitaria"] = "Informació Sanitària";
	$lang["MalaltSovint"] = "Es posa malalt sovint?";
	$lang["MalaltSovintDetall"] = "Malalties més freqüents";
	$lang["PrenMedicament"] = "Pren algun medicament?";
	$lang["PrenMedicamentDetall"] = "Quin?";
	$lang["PrenMedicamentAdministracio"] = "Administració";
	$lang["SotaTractament"] = "Està sota algun tratament específic?";
	$lang["SotaTractamentDetall"] = "Quin?";
	$lang["Regim"] = "Està sota algun règim?";
	$lang["RegimDetall"] = "Quin?";
	$lang["DesordreAlimentari"] = "Pateix algun tipus de trastorn d'ordre alimentari?";
	$lang["DesordreAlimentariDetall"] = "Quin?";
	$lang["Operat"] = "Ha sigut intervingut quirúrgicament?";
	$lang["OperatDetall"] = "Especificar";
	/*
	$lang["Alergic"] = "És al•lèrgic?";
	$lang["AlergicDetall"] = "Especificar";
	*/
	$lang["Alergic"] = "Al•lèrgies:";
	$lang["Alergic_celiac"] = "Celiaquia?";
	$lang["Alergic_lactosa"] = "Lactosa?";
	$lang["Alergic_ou"] = "A l'ou?";
	$lang["Alergic_altres"] = "Altres?";
	$lang["Alergic_Detall"] = "Especificar (Animals, insectes...):";

	$lang["H_InteresGeneral"] = "Dades d'interès general";

	/* $lang["AltresMateixTipus"] = "Has participat en altres estades d'aquest tipus?"; */
	$lang["AltresMateixTipus"] = "És la primera vegada que participes en colònies d'estiu?";
	$lang["Nedar"] = "Saps Nedar?";
	$lang["PorAigua"] = "Tens por a l'aigua?";
	$lang["Bicicleta"] = "Saps anar en bicicleta?";
	$lang["AnglesForaEscola"] = "Estudies anglès fora de l'escola?";
	$lang["AnglesForaEscolaDetall"] = "Quin curs?";
	$lang["Vertigen"] = "Tens Vertigen?";
	$lang["DificultatSports"] = "Tens alguna dificultat per practicar algun esport?";
	$lang["DificultatSports_Quins"] = "Quin?";
		
		
	$lang["H_LPD"] = "Protecció de dades";
	$lang["LPD"] = "L'informem que totes les dades de caràcter personal a les que es refereix aquest formulari, fins i tot la informació sanitària, seran tractades amb l'objectiu de facilitar la gestió de les activitats d’educació en el lleure en les quals participen menors de 18 anys.<br /><br />L'interessat/da, o el seu representant legal, autoritzen expressament a Colònies RV SA, al tractament de les dades amb aquesta finalitat.<br /><br />Tanmateix, l'informem de la possibilitat d'exercir, en termes establerts en la Llei Orgànica de Protecció de Dades de Caràcter Personal (LOPD), els drets d'accés, rectificació, cancel•lació i oposició, adreçant-se a Colònies RV SA; C/ Diputació, núm. 238, entresol 3ª, Barcelona, on  se li facilitaran els impresos oficials oportuns.";

	$lang["H_AutoritzacioPersonal"] = "Autorització (per a menors de 18 anys)";
	$lang["Autoritzacio_senyor"] = "Senyor/a";
	$lang["Autoritzacio_dni"] = ", amb DNI";
	$lang["Autoritzacio_nom_fill"] = "autoritza al seu fill/a";
	$lang["Autoritzacio_del"] = "a assistir als campaments del";
	$lang["Autoritzacio_al"] = "al";
	$lang["Autoritzacio_casa_colonies"] = "a la casa de colònies";
	$lang["Autoritzacio_poblacio_casa_colonies"] = "de";
	$lang["Autoritzacio_compromis"] = "Faig extensiva aquesta autorització a les decisions medicoquirúrgiques  que fossin necessàries adoptar en cas d'extrema urgència, sota la pertinent acció facultativa. Així mateix autoritzo, que en cas de malaltia o indisposició, el meu fill/a sigui traslladat de visita al centre de salut més proper. Autoritzo la utilització de qualsevol material fotogràfic o audiovisual en que aparegui el participant per a futures promocions de RV.";
	$lang["Autoritzacio_acceptacio"] = "Marca la casella conforme acceptes els termes explicats en aquest document.";
	$lang["Autoritzacio_poblacio"] = "Accepto en (població)";

	$lang["H_Acceptacio_Normativa"] = "Acceptació de la Normativa de Funcionament";
	$lang["Normativa_autoritzacio"] = "Marca la casella conforme acceptes els termes explicats en la Normativa.";
	$lang["Normativa_acceptacio"] = "He llegit i Accepto les condicions detallades a la Normativa de funcionament.";
	$lang["Normativa_llegir"] = "Llegir les";
	$lang["Normativa_texte_enllac"] = "condicions";
	$lang["Normativa_enllac_pdf"] = "./pdf/normativa_ca.pdf";
		
	$lang["H_Botonera"] = "Formalització de la inscripció";
	$lang["Botonera"] = "Un cop introduïdes les dades premi el botó adient per continuar amb el procés d’inscripció.";
	$lang["SyC"] = "Salvar les dades del participant";
	$lang["NEW"] = "Introduir un nou Participant sense Salvar";
	$lang["UPD"] = "Actualitzar el Participant actual";
	$lang["END"] = "Posar fi a la Inscripció";

	/*variables introducidas por Juanma*/
	$lang["titulo_hipica"] = "Enquesta H&iacute;pica";
	$lang["cabecera_hipica"] = "Dades d&#39inter&egrave;s per l&#39h&iacute;pica:";
	$lang["q_1_hipica"] = "Has muntat a cavall anteriorment?";
	$lang["hipica1_radio1"] = "No.";
	$lang["hipica1_radio2"] = "Si, una experi&egrave;cia positiva.";
	$lang["hipica1_radio3"] = "Si, una experi&egrave;cia negativa.";
	$lang["comentario_hipica"] = "En cas d&#39haver marcat la primera opci&oacute;, no cal que responguis les seg&uuml;ents q&uuml;estions.";
	$lang["q_2_hipica"] = "Quantes vegades has muntat, aproximadament?";
	$lang["hipica2_radio1"] = "Entre 1 i 5 vegades.";
	$lang["hipica2_radio2"] = "Entre 5 i 10 vegades.";
	$lang["hipica2_radio3"] = "M&eacute;s de 10 vegades.";
	$lang["q_3_hipica"] = "Has trotat alguna vegada?";
	$lang["hipica3_radio1"] = "No, mai.";
	$lang["hipica3_radio2"] = "He trotat, per&ograve; no s&eacute; fer-ho.";
	$lang["hipica3_radio3"] = "He trotat, i s&eacute; fer el trot aixecat o angl&egrave;s";
	$lang["q_4_hipica"] = "Has galopat alguna vegada?";
	$lang["hipica4_radio1"] = "No, mai.";
	$lang["hipica4_radio2"] = "He galopat, per&ograve; no s&eacute; fer-ho.";
	$lang["hipica4_radio3"] = "He galopat, i s&eacute; fer el moviment correcte.";
	$lang["comentario_observaciones_hipica"] = "Si creus que hi ha alguna dada m&eacute;s (classes rebudes, acc&eacute;s a cavalls propis o d&#39amics,etc...), fes-ho constar en l&#39apartat d&#39observacions.)";
	$lang["observaciones_hipica"] = "Observacions: especifiqueu en aquest apartat si sou repetidors de programa d'equitació.";

	$lang["titulo_grupo7"] = "Autoritzaci&oacute; per paintball, motos i quad.";
	$lang["autorizacion_grupo7"] = "Autoritzo al meu fill/a a practicar les activitats de paintball, motos i quad durant la seva estada a les col&ograve;nies de Rosa dels Vents.";
	$lang["exp_grupo7"] = "Marca la casella conforme autoritzes al teu fill/a.";
	/*Fin variables Juanma*/	


	$lang["EnlaceBorrar"] = "X";

	$lang["ObsGeneral"] = "Altres aspectes a tenir en compte (insomni, sonambulisme...):";

	$lang["H_Mallorca"] = "DNI obligatori";
	$lang["exp_mallorca"] = "Per a aquest programa es requisit indispensable posar el DNI del participant. Aquest DNI no pot estar caducat.";

	$lang["H_futbol"] = "Campus de Futbol";
	$lang["exp_futbol"] = "Escull l'equipament que vols lluir al curs de futbol";
	$lang["Portero"] = "Necessitaré equipament de Porter";
	$lang["Jugador"] = "Necessitaré equipament de Jugador de camp";

	$lang["H_Informacio"] = "Com complimentar aquest formulari";
	$lang["Informacio_Presentacion"] = "Aquest petit text esta destinat a ajudar-lo a complimentar les dades d'aquest formulari.";
	$lang["Informacio_Basica"] = "El primer a tenir en compte es que s'ha d'omplir un formulari per cada un dels participants de la reserva.".
							"Per a introduir aquestes dades serà necessari omplir tots els camps que trobarà en el formulari que te davant seu.<br/><br/>".
							"Els únics camps que podrà deixar buits, son els camps d'Observacions generals.<br/><br/>".
							"Un cop introduïdes les dades al formulari haurà de prémer el boto <b>".$lang["SyC"]."</b>.<br/><br/>".
							"Un cop Salvada la informació de la seva primera fitxa de participant, apareixerà un quadre en la part superior de la plana amb el llistat dels participants introduïts. Si ha de introduir mes fitxes podrà continuar introduint les dades en el formulari que haurà quedat sota la caixa de <b>".$lang["H_Resumen"]."</b>.<br/>";
	$lang["Informacio_Update"] = "En cas que volgués modificar la informació introduïda, podrà fer-ho clickant sobre la foto o el nom dels participants que trobarà a la caixa de <b>".$lang["H_Resumen"]."</b><br/>";
	$lang["Informacio_EncuestasPendientes"] = "Com pot comprovar a la caixa de <b>".$lang["H_Resumen"]."</b> te fitxes pendents de finalitzar la <b>Prova de Nivell d'Angles</b>. Per accedir-hi nomes ha de fer click sobre el text <b>".$lang["EnquestaPendent"]."</b><br />";
	$lang["Informacio_BtnFinalizar"] = "Si no ha d'introduir cap fitxa mes i no te cap alerta pendent, ja pot prémer el botó de <b>".$lang["END"]."</b> per finalitzar el procés.<br/>";

	$lang["H_ErrorFechas"] = "Error de Dades";
	$lang["MsgErrorFechas"] = "No ha sigut possible actualitzar les dades. Si us plau, torni a realitzar els canvis.";
		
	$lang["texto_adjuntar_1"] = "Per adjuntar un fitxer arrosegal aquí, o ";
	$lang["texto_adjuntar_2"] = "escull un aquí";
	$lang["MensajeNoExiste"] = "Atenció, la Fitxa d'inscripció a la que intenta accedir ha deixat d'existir. Torni a accedir a la seva reserva per continuar amb el proces d'inscripció.";	
	$lang["finalizada_correctamente"] = "La Fitxa d'Inscripci&oacute; ha sigut finalitzada correctament.";
	$lang["obligatorio"] = "Dada obligatoria";
	$lang["campos_obligatorios_pendientes"] = "Queden dades obligatòries pendents d'introduir";
	
	?>	