<?php
    // $lang[""] ="";

    $lang["TitolWeb"] = "Enquesta de validació de nivell d'anglès";
    $lang["Titol"] = "Enquesta de validació de nivell d'anglès";
    $lang["NReserva"] = "Número Reserva";
    $lang["Localitzador"] = "Localitzador";
    $lang["Dni"] = "DNI";
    $lang["Participant"] = "Participant";

    $lang["H_Titulo"] = "Level Test";
    $lang["H3_Enunciado"] = "Select the correct answer. Only ONE answer is correct";

    $lang["boton"] = "Enviar la Enquesta";
    $lang["AlertaEnviament"] = "Esta segur que vol enviar la enquesta? un cop enviada no podrà tornar a accedir-hi.";
    $lang["AlertaFaltanCampos"] = "Per poder continuar has de respondre totes les preguntes";
    $lang["PruebaNoRealizada"] = "Prova de nivell No Realitzada";
    
    $lang["True"] = "Sí";
    $lang["False"] = "No";
    $lang["val_True"] = 1;
    $lang["val_False"] = 0;
    
    $lang["Idioma"] = "Idioma";
    $lang["Idioma1"] = "Catalán";
    $lang["Idioma2"] = "Castellano";
    
    $lang["H_Info"] = "Informació addicional";
    $lang["Info_Descripcion"] = "A continuació podreu respondre a les qüestions de la següent prova de nivell. El motiu del qüestionari es separar als participants en tres nivells d’anglès: Bàsic, Intermig i Alt. <br /> <br /><b style='color:firebrick;''>ATENCI&Oacute;: El test no es pot repetir, si us plau, repasseu bé les seves respostes abans de finalitzar.</b><br /> <br />En el cas de què no vulgueu contestar les preguntes i quedar inscrits en el nivell Bàsic clicar aquí:";
    $lang["NSNC"] = "Desitjo pertanyer al nivell Bàsic";
    
    $lang["AciertosNSNC"] = "<br/><br/>Desitjo pertanyer al <b>nivell Bàsic</b>";
    $lang["MensajeFinalizadoTest"] = "La prova de nivell ha sigut completada correctament.";
    $lang["MensajeTestOutDated"] = "Atenció, ya no es possible accedir al test de nivell.<br/>El període per realitzar el test de nivell es des del día de la<br/>reserva fins a 15 díes abans de l'inici del curs.<br/>Els participants que no hagin pogut fer el test de nivell on-line<br/>el faràn en l'escuela el primer día.";
    $lang["NoFound"] = "No se ha encontrado ninguna reserva con estos datos identificativos.<br />Por favor inténtelo de nuevo pasado 24 horas<br />(es posible que aun no haya sido procesada por el sistema).";
    $lang["AlreadyDone"] = "Prova de nivell realitzada amb anterioritat.";
    $lang["MensajeNoExiste"] = "Atenció, el test de nivell al que intenta accedir no existeix.";
	

    $lang["Splitter"]   ="|@|";
    $lang["npreguntas"] = 20;
    $lang["Aciertos"] = "<br/><br/>¡Ha encertat <b>{respuestas}/".$lang["npreguntas"]." respostes!</b>";
    $lang["Textaxo"]    =   "How old are you?".$lang["Splitter"].
                            "I have 9 years old".$lang["Splitter"].
                            "I am 9 years old".$lang["Splitter"].
                            "I have 9 years".$lang["Splitter"].
                            
                            "_______ are you from?".$lang["Splitter"].
                            "Who".$lang["Splitter"].
                            "What".$lang["Splitter"].
                            "Where".$lang["Splitter"].
                            
                            "Where is the book? – The book is _______ the table".$lang["Splitter"].
                            "At".$lang["Splitter"].
                            "On".$lang["Splitter"].
                            "In".$lang["Splitter"].
                            
                            "How long have you been living in your town?".$lang["Splitter"].
                            "For 7 years".$lang["Splitter"].
                            "Since 7 years".$lang["Splitter"].
                            "At 7 years".$lang["Splitter"].
                            
                            "She plays basketball _______ Mondays.".$lang["Splitter"].
                            "In".$lang["Splitter"].
                            "On".$lang["Splitter"].
                            "At".$lang["Splitter"].
                            
                            "We haven’t got _______ food in the fridge.".$lang["Splitter"].
                            "Some".$lang["Splitter"].
                            "Very".$lang["Splitter"].
                            "Any".$lang["Splitter"].
                            
                            "They are going to France with _______ son".$lang["Splitter"].
                            "There".$lang["Splitter"].
                            "Here".$lang["Splitter"].
                            "Their".$lang["Splitter"].
                            
                            "I _______ to the supermarket at the moment.".$lang["Splitter"].
                            "Am going ".$lang["Splitter"].
                            "Go ".$lang["Splitter"].
                            "Went".$lang["Splitter"].
                            
                            "My suitcase is heavy, but yours is _______".$lang["Splitter"].
                            "More heavy".$lang["Splitter"].
                            "Heavier".$lang["Splitter"].
                            "Heavyer".$lang["Splitter"].
                            
                            "What time _______ to school every day?".$lang["Splitter"].
                            "Do you go".$lang["Splitter"].
                            "You go".$lang["Splitter"].
                            "Are you going".$lang["Splitter"].
                            
                            "Where _______ you go yesterday?".$lang["Splitter"].
                            "Do".$lang["Splitter"].
                            "Are".$lang["Splitter"].
                            "Did".$lang["Splitter"].
                            
                            "What _______ he like to do?".$lang["Splitter"].
                            "Does".$lang["Splitter"].
                            "Do".$lang["Splitter"].
                            "Has".$lang["Splitter"].
                            
                            "Where is Susan? - She’s _______ holiday.".$lang["Splitter"].
                            "In".$lang["Splitter"].
                            "On".$lang["Splitter"].
                            "By".$lang["Splitter"].
                            
                            "What _______ you like to drink?".$lang["Splitter"].
                            "Will".$lang["Splitter"].
                            "Could".$lang["Splitter"].
                            "Would".$lang["Splitter"].
                            
                            "That car is _______ than mine.".$lang["Splitter"].
                            "More fast ".$lang["Splitter"].
                            "Faster".$lang["Splitter"].
                            "Most fast".$lang["Splitter"].
                            
                            "Anne _______ in an office.".$lang["Splitter"].
                            "Working".$lang["Splitter"].
                            "Work".$lang["Splitter"].
                            "Works".$lang["Splitter"].
                            
                            "It’s _______ expensive apple.".$lang["Splitter"].
                            "One".$lang["Splitter"].
                            "A".$lang["Splitter"].
                            "An".$lang["Splitter"].
                            
                            "_______ cat is black".$lang["Splitter"].
                            "Her".$lang["Splitter"].
                            "They".$lang["Splitter"].
                            "Him".$lang["Splitter"].
                            
                            "_______ books are those?".$lang["Splitter"].
                            "Whose".$lang["Splitter"].
                            "Why".$lang["Splitter"].
                            "Who".$lang["Splitter"].
                            
                            "What are you waiting _______?".$lang["Splitter"].
                            "At ".$lang["Splitter"].
                            "On".$lang["Splitter"].
                            "For".$lang["Splitter"];
?>