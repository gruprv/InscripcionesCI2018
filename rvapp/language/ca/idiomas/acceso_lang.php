<?php
	$lang["TitolWeb"] = "Accés a la ficha d'inscripció.";
	$lang["Titol"] = "Accés a la ficha d'inscripció.";
	$lang["NReserva"] = "Número Reserva";
	$lang["Localitzador"] = "Referència";
	$lang["Dni"] = "DNI";
	$lang["formatDni"] = "Ex: 00000000A";
	$lang["centro"] = "Centre";
	$lang["programa"] ="Programa";
	$lang["BtnSubmit"] = "Accedir al sistema";
	$lang["Idioma"] = "Idioma";
	$lang["Idioma1"] = "Català";
	$lang["Idioma2"] = "Castellano";
	$lang["atencion_no_reserva"] 	= "	<center>
											<h1>
												Informació Important:<br/><br/>
												Està a punt d'entrar al Full d'Inscripció<br/>
												de Rosa dels Vents Idiomes.
											</h1>
										</center><br/><br/>
										<div style='text-align: left;'>
											En aquest document podrà ampliar l'informació sobre el seu fill/a, i així podrem oferir-li un millor servei.<br/><br/>
											Si us plau, sigui molt exhaustiu amb la informació sanitària, medicaments, al·lèrgies, etc. <br/><br/>
											Un cop finalitzat el Full d'Inscripció no podrà tornar a accedir-hi. Si desitgen ampliar o modificar alguna dada: canvi de medicació, canvi de dates, o qualsevol altre informació que vostè consideri important, si us plau enviï un email a: <a href='mailto://info@rosadelsventsidiomas.es'>info@rosadelsventsidiomas.es</a>, necessitarem:<br/>
											<ul>
												<li>Número de la reserva</li>
												<li>Nom i cognoms de l'estudiant</li>
												<li>Dates de sortida i arribada</li>
												<li>Informació addicional que desitgi afegir o modificar</li>
											</ul>
											Aquest Full d'Inscripció no dóna cap dret a plaça si prèviament no s'ha realitzat el pagament.<br/><br/>
										</div>";
	$lang["NoFound"] = "No s'ha trovat cap reserva amb aquestes dades identificatives.<br />Si us plau intenti-ho de nou en 24 hores<br />(Es possible que encara no hagi sigut procesada pel sistema).";
	$lang["titulo_acceso"] = "Accés a l'inscripció";
	$lang["Texto_aplazado1"] = "El formulari d'Inscripció als cursos d'idiomes estarà disponible a partir de la data: ";
	$lang["Texto_aplazado2"] = ", si us plau torni passat aquesta data per completar el procés de inscripció dels seus fills/es.";
	$lang["finaliza_correctamente"] = "La reserva ha finalitzat.";
	$lang["finaliza_despedida"] = "Gràcies per la seva atenció i moltes gràcies per confiar en Rosa dels Vents.";
	$lang["titulo_info"] = "Important.";
	$lang["test_nom"] = "Nom i cognoms del participant";
	$lang["finaliza_test_correctamente"] = "Ha finalitzat el test.";
	$lang["Titol_test"] = "Accés al test de nivell d'anglès.";
	$lang["MensajeFinalizado"] = "El Full de Inscripció ha sigut completada correctament.";
	$lang["MensajeFinalizadoTest"] = "La Prova de Nivell ha sigut completada correctament.";
	$lang["MensajeNoExiste"] = "Atenció, el Full de Inscripció / Prova de Nivell a la que intenta accedir ha deixat d'existir. Si us plau, torni a posar les seves dades en el formulari d’accés.";
	// $lang["MensajeYaFinalizado"] = "Atenció, el Full de Inscripció / Prova de Nivell a la que intenta accedir ja ha sigut Finalitzada amb anterioritat. Si us plau, si necesita fer alguna modificació truqui al Servei d'Atenció al Client de Rosa dels Vents Idiomes al telefón <b>935.036.035</b> o envíi un email a <a href='mailto:info@rosadelsventsidiomas.es'><b>info@rosadelsventsidiomas.es</b></a>.";
	$lang["MensajeYaFinalizado"] = "La Fitxa d’Inscripció a la que intenta accedir ja ha sigut finalitzada amb anterioritat.<br/><br/>Si necessita fer alguna modificació, li agrairem ens envií un email  al Servei d’Atenció al client de Rosa dels Vents a: <a href='mailto:info@rosadelsventsidiomas.es'><b>info@rosadelsventsidiomas.es</b></a>.";
	$lang["MensajeTestOutDated"] = "Atenció, ja no és possible accedir ni al Test de Nivell ni a la Fitxa d'Inscripció.<br/><br/>El període per a realitzar aquests tramits finalitza 30 dies despres de formalitzar la reserva.<br/><br/>Els participants que no hagin pogut fer el test de nivell on-line el faran a l'escola el primer dia.";
	$lang["atencion_no_reserva2"]   = "";
	$lang["MensajeDe1En1"] = "Atenció: Ja te un <u>Full d'inscripció</u> o <u>Prova de nivell</u> oberta en aquest navegador.<br/><br/>Si us plau, finalitzi-la abans de continuar amb les següents.<br/><br/>Si la ha tancat i continua veient aquest missatge, si us plau, tanqui aquest navegador i torni a entrar.<br/><br/>Disculpi les molesties.";
?>