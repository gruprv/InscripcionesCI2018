<?php
/**
 * CodeIgniter
 *
 * An open source application development framework for PHP
 *
 * This content is released under the MIT License (MIT)
 *
 * Copyright (c) 2014 - 2017, British Columbia Institute of Technology
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 *
 * @package	CodeIgniter
 * @author	EllisLab Dev Team
 * @copyright	Copyright (c) 2008 - 2014, EllisLab, Inc. (https://ellislab.com/)
 * @copyright	Copyright (c) 2014 - 2017, British Columbia Institute of Technology (http://bcit.ca/)
 * @license	http://opensource.org/licenses/MIT	MIT License
 * @link	https://codeigniter.com
 * @since	Version 1.0.0
 * @filesource
 */
defined('BASEPATH') OR exit('No direct script access allowed');

$lang['imglib_source_image_required'] = 'Has de especificar una imatge d\'origen a les teves preferencies.';
$lang['imglib_gd_required'] = 'La llibreria GD image es un requisit per aquesta necesitat.';
$lang['imglib_gd_required_for_props'] = 'El teu servidor ha de suportar la llibreria GD image per poder determinar les propietats de la imatge.';
$lang['imglib_unsupported_imagecreate'] = 'El teu servidor no soporta la funció GD requerida per processar aquest tipus d\'imatge.';
$lang['imglib_gif_not_supported'] = 'Les imatges tipus GIF no solen estar soportades per temes de llicencia. Deus fer servir fitxers JPG o PNG.';
$lang['imglib_jpg_not_supported'] = 'Les imatges tipus JPG no estan soportades.';
$lang['imglib_png_not_supported'] = 'Les imatges tipus PNG no estan soportades.';
$lang['imglib_jpg_or_png_required'] = 'El protocol de canvi de tamany especificat en les preferencies nomes funciona amb fitxers de tipus JPEG o PNG.';
$lang['imglib_copy_error'] = 'S\'ha produit un error mentres intentavem canviar el fitxer. Si us plau, asegureu que el fitxer permet escriptura.';
$lang['imglib_rotate_unsupported'] = 'El servidor no permet rotar la imatge..';
$lang['imglib_libpath_invalid'] = 'La ruta a la llibreria de imatges no es correcta. Si us plau, estableix un path correcte en les preferencies d\'imatge.';
$lang['imglib_image_process_failed'] = 'Ha fallat el processat de la imatge. Si us plau, verifiqueu que el servidor soporta el protocol escollit i quela ruta de la llibreria d\'imatge es correcta.';
$lang['imglib_rotation_angle_required'] = 'Es necessita un angle per a poder rotar la imatge.';
$lang['imglib_invalid_path'] = 'La ruta per a la imatge es incorrecte.';
$lang['imglib_invalid_image'] = 'La imatge no es correcte.';
$lang['imglib_copy_failed'] = 'La rutina de copia d\'imatge ha fallat.';
$lang['imglib_missing_font'] = 'No es possible trobar una font.';
$lang['imglib_save_failed'] = 'No es possible guardar la imatge. Si us plau assegureu que la imatge i el directori son modificables.';
